This tool calculates the size of all the files under a folder path including the files in sub folders. 
I wrote this code some time back to find out where is my space being consumed when I was low on HDD space. 
As there is no native tool on Windows to do this, I wrote my own. This is a command line tool without any UI but it gets the job done. 

It supports following command line arguments:
  -p Folder Path to calculate total size. e.g. FolderSize.exe -p \"F:\\Learning\\Development\"
  -d Show folder size for debug purpose.
  -s Save folder size(s) to a file. No need give a filename for this argument. 
