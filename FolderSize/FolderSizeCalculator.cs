﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSize
{
    public class FolderSizeCalculator
    {
        readonly string folderPath = "";

        // Private Field
        Dictionary<string, long> folderSizeDictionary = new Dictionary<string, long>();

        // To update current progress
        public delegate void PrintCurrentFolderSize(string folderName, long folderSize);
        public event PrintCurrentFolderSize OnPrintCurrentFolderSize;

        // To update errors
        public delegate void PrintCurrentError(string errStr);
        public event PrintCurrentError OnPrintCurrentError;

        // Public Properties
        public long Folders { get; private set;}
        public long Files { get; private set; }


        // Constructor to get input path
        public FolderSizeCalculator(string path)
        {
            if (string.IsNullOrEmpty(path) == true)
                throw new Exception("Empty Folder Path");

            if (Directory.Exists(path) == false)
                throw new Exception("'" + path + "' doesnot exist.");

            folderPath = path;
        }


        // Calculate the size for given folder 
        public long Calculate()
        {
            try
            {
                long folderSize = 0;

                var task = Task.Run(() =>
                {
                    folderSize = CalculateFolderSize(new DirectoryInfo(folderPath));
                });

                task.Wait();
                return folderSize;
            }
            catch
            {
                throw;
            }
        }


        // Actual work horse method
        private long CalculateFolderSize(DirectoryInfo dirInfo)
        {
            long folderSize = 0;

            try
            {                
                FileInfo[] fileInfoArray = dirInfo.GetFiles();

                foreach (FileInfo fi in fileInfoArray)
                {
                    folderSize += fi.Length;
                }

                Files += fileInfoArray.Length;

                // Add subdirectory sizes.
                DirectoryInfo[] dis = dirInfo.GetDirectories();

                foreach (DirectoryInfo di in dis)
                {
                    folderSize += CalculateFolderSize(di);
                }

                Folders += dis.Length;
                // Do we have a consumer for this event?
                OnPrintCurrentFolderSize?.Invoke(dirInfo.FullName, folderSize);
                folderSizeDictionary.Add(dirInfo.FullName, folderSize);                
            }
            catch (Exception exp)
            {
                OnPrintCurrentError?.Invoke(exp.Message);                
            }

            return folderSize;
        }


        // Save folder size report data to output file
        internal string SaveFolderSizeDataToFile()
        {
            var fileName = GetNewFileName();

            using (StreamWriter writer = new StreamWriter(fileName))
            {

                folderSizeDictionary.Keys.ToList().ForEach(key =>
                {
                    writer.WriteLine("{0},{1}", key, folderSizeDictionary[key].ToReadableFileSize());
                });
                
                writer.Close();
            }

            return fileName;
        }


        // Generate a fairly unique file name.
        private string GetNewFileName()
        {
            //BK_ToDo: Check if this file name already exists - just to be sure 
            return "FolderSize_Report_" + DateTime.Now.ToString("yyyyMMdd-HH-mm-ss") + ".csv";
        }
    }
}
