﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FolderSize
{
    class Program
    {
        // Get program name from the assembly
        public static string ProgramName
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Name;
            }
        }

        static void Main(string[] args)
        {
            bool bShowDebugInfo = false;
            bool bSaveToFile = false;
            string path = "./"; // default path

            // Check what we have got on command line
            for(int i = 0; i < args.Length; i++)
            {
                if (args[i] == "?" || args[i] == "-h" || args[i] == "-help")
                {
                    ShowHelp();
                    return;
                }
                else if (args[i] == "-d") // Show debug while processing
                    bShowDebugInfo = true;
                else if (args[i] == "-s") // Save path and their size in the output file.
                    bSaveToFile = true;
                else if (args[i] == "-p") // Path to process
                {
                    path = args[i + 1];
                    i++;
                }
                else
                {
                    Console.WriteLine("Invalid argument '{0}'.", args[i]);
                    return;
                }
            }

            // Do the actual work
            Console.WriteLine("Calculating folder size for '{0}'.", path);

            try
            {
                FolderSizeCalculator folderSizeCalculator = new FolderSizeCalculator(path);

                folderSizeCalculator.OnPrintCurrentError += FolderSizeCalculator_OnPrintCurrentError;
                if (bShowDebugInfo == true) // Show it on the command prompt
                    folderSizeCalculator.OnPrintCurrentFolderSize += FolderSizeCalculator_OnPrintCurrentFolderSize;

                var size = folderSizeCalculator.Calculate();

                // Work is done. 
                Console.WriteLine("\nTotal Size --> {0}  ({1} bytes) across {2} files and {3} folders.", size.ToReadableFileSize(), size.ToString("N0"), folderSizeCalculator.Files.ToString("N0"), folderSizeCalculator.Folders.ToString("N0"));

                // Check if we should save the report
                if (bSaveToFile == true)
                {
                    var fileName = folderSizeCalculator.SaveFolderSizeDataToFile();
                    Console.WriteLine("'{0}' created.", fileName);
                }
            }
            catch(Exception exp)
            {
                // We are here because of some unexpected situation. Handle it as gracefully as possible. 
                if(exp.InnerException == null)
                    Console.WriteLine(exp.Message);
                else
                    Console.WriteLine(exp.InnerException.Message);

                Console.WriteLine(" ");
                ShowHelp();
                return;
            }

        }

        private static void FolderSizeCalculator_OnPrintCurrentError(string errStr)
        {
            Console.WriteLine("{0}", errStr);
        }

        private static void FolderSizeCalculator_OnPrintCurrentFolderSize(string folderName, long folderSize)
        {
            Console.WriteLine("{0}: {1}", folderName, folderSize.ToReadableFileSize());
        }


        #region Utility Methods

        // 
        private static void ShowHelp()
        {            
            Console.WriteLine(GetNameVersionInfo());
            Console.WriteLine("  -p Folder Path to calculate total size. e.g. {0} -p \"F:\\Learning\\Development\"", ProgramName);
            Console.WriteLine("  -d Show folder size for debug purpose.");
            Console.WriteLine("  -s Save folder size(s) to a file. No need give a filename for this argument.");
        }


        // Get product name, copyrights and version information etc.
        private static string GetNameVersionInfo()
        {
            StringBuilder sb = new StringBuilder();

            string copyrights = "";
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length == 0)
                copyrights = "";
            else
                copyrights = ((AssemblyCopyrightAttribute)attributes[0]).Copyright;

            sb.Append(ProgramName);
            sb.Append(" ");
            sb.Append(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion);
            sb.AppendLine(" ");
            sb.Append(copyrights);

            return sb.ToString();
        }

        #endregion 
    }
}
