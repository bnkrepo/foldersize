﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSize
{
    public static class Extensions
    {
        static double KB = 1024.0;

        // Extension method to convert size to appropriate format.
        public static string ToReadableFileSize(this long value)
        {
            double MB = KB * KB;
            double GB = MB * KB;

            string size = "0 Bytes";

            if (value >= GB)
                size = string.Format("{0:##.##}", value / GB) + " GB";
            else if (value >= MB)
                size = string.Format("{0:##.##}", value / MB) + " MB";
            else if (value >= KB)
                size = string.Format("{0:##.##}", value / KB) + " KB";
            else if (value > 0 && value < KB)
                size = value.ToString() + " Bytes";

            return size;
        }
    }
}
